# master-git

Repository for learning basic VCS using git. Proper knowledge saves a lot of time, and makes developement process much easier, so please pay extra attention when reading and excercising.

## Basics
[(Documentation)](https://git-scm.com/doc)

[Basic git knowledge](https://learnxinyminutes.com/docs/git/)

[Git workflows](https://www.atlassian.com/git/tutorials/comparing-workflows)

[Git cheatsheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet/)

## Exercises
[Udemy course](https://www.udemy.com/course/github-ultimate/)

[Learn branching](http://learngitbranching.js.org/)

[Excercise set 1 (Easy - Medium difficulty)](https://github.com/jlord/git-it-electron#what-to-install)

[Excercise set 2 (Easy - Hard difficulty)](http://gitexercises.fracz.com/exercise/master)

## PyCharm integration
PyCharm is a powrful IDE used in our place of work. It has a great git implementation, you can learn more about it [here](https://www.youtube.com/watch?v=jFnYQbUZQlA).

## Utils:
[Git ignore generator](https://www.gitignore.io/api/osx,python,pycharm,django,)

Bash alias for tool above:
```
function gi() { curl -L -s https://www.gitignore.io/api/$@ ;}
```

## Useful aliases
```
[alias]
	glog = log --date-order --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --abbrev-commit
	l = log --date-order --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --abbrev-commit
	ls = log --date-order --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --abbrev-commit --all
	up = rebase
    sl = stash list
    sa = stash apply
    ss = stash save
    r = reset
    r1 = reset HEAD^
    r2 = reset HEAD^^
    rh = reset --hard
    rh1 = reset HEAD^ --hard
    rh2 = reset HEAD^^ --hard
    cp = cherry-pick
    st = status
    cl = clone
    ci = commit
    co = checkout
    br = branch 
    diff = diff --word-diff
    dc = diff --cached
    la = "!git config -l | grep alias | cut -c 7-"
```

## SSH
[SSH Essentials](https://www.digitalocean.com/community/tutorials/ssh-essentials-working-with-ssh-servers-clients-and-keys)

[Adding a new SSH key to your GitHub account](https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/)